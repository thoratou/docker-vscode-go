.PHONY: all

build:
	@CONTAINER_IMAGE=registry.gitlab.com/thoratou/docker-vscode-go \
	CI_COMMIT_SHA=dontcare \
	/bin/sh scripts/build.sh

push:
	@CONTAINER_IMAGE=registry.gitlab.com/thoratou/docker-vscode-go \
	CI_COMMIT_SHA=dontcare \
	/bin/sh scripts/push.sh

all: build push

